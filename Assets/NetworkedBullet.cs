using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class NetworkedBullet : MonoBehaviourPun
{
    public float Speed = 5;

    // Start is called before the first frame update
    void Start()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            photonView.TransferOwnership(PhotonNetwork.MasterClient);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine)
            transform.position += (transform.forward * Speed) * Time.deltaTime;
    }
}
