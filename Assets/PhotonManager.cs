using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;
using System.Text;
using System;
using Photon.Chat;
using ExitGames.Client.Photon;
using UnityEngine.UI;

public class PhotonManager : MonoBehaviourPunCallbacks, IOnEventCallback
{
    private byte maxPlayers;
    [SerializeField] string desiredRoom = "";
    List<RoomInfo> roomInfos;
    public const byte MoveUnitsToTargetPositionEventCode = 1;
    public GameObject PlayerPrefab;
    public List<Transform> SpawnPoints;
    public InputField roomInputField;
    public Text roomTextDisplay;
    public GameObject Error; 
    public Text errorTextDisplay;



    void Start()
    {
        ConnectToMasterServer();

    }

    //=========================================
    //
    //=========================================
    public void ConnectToMasterServer()
    {

        if (!PhotonNetwork.IsConnected)
        {
            //Connect to server
            PhotonNetwork.ConnectUsingSettings();
            Debug.Log("Connecting to Photon server");
        }
        else
        {
            Debug.Log("We are connected already");
        }
    }

    //=========================================
    //
    //=========================================
    public override void OnConnectedToMaster()
    {
        TypedLobby PrivateLobby = new TypedLobby("Private", LobbyType.Default);
        PhotonNetwork.JoinLobby(TypedLobby.Default);
        Debug.Log("Connected to lobby");
    }

    public override void OnErrorInfo(ErrorInfo errorInfo)
    {
        base.OnErrorInfo(errorInfo);
        Debug.Log("WE  got a general erro while connecting " + errorInfo.Info);

    }
    //=========================================
    //
    //=========================================
    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();

        Debug.Log("WE are in a Lobby: " + PhotonNetwork.CurrentLobby.Name);


    }

    //=========================================
    //
    //=========================================
    public override void OnJoinedRoom()
    {
        Debug.Log("Room joined ");

        GameObject g = PhotonNetwork.Instantiate(PlayerPrefab.name, SpawnPoints[PhotonNetwork.CurrentRoom.PlayerCount - 1].position, SpawnPoints[PhotonNetwork.CurrentRoom.PlayerCount - 1].rotation);
        g.name = " Player" + PhotonNetwork.CurrentRoom.PlayerCount;
       

    }
   
    public void JoinRoomByName()
    {
        if (PhotonNetwork.InLobby)
        {
            PhotonNetwork.JoinRoom(roomInputField.text);
        }


    }

    //=========================================
    //
    //=========================================


    public override void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    public override void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }
    public float roomeventFreq = 2500;
    float timer;
    private void Update()
    {
        if (PhotonNetwork.InRoom)
        {
            timer += Time.deltaTime;
            if (timer >= roomeventFreq)
            {
                object[] content = new object[] { new Vector3(10.0f, 2.0f, 5.0f), 1, 2, 5, 10 }; // Array contains the target position and the IDs of the selected units
                RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                PhotonNetwork.RaiseEvent(MoveUnitsToTargetPositionEventCode, content, raiseEventOptions, SendOptions.SendReliable);
                timer = 0;
            }
        }
    }
    private void SendMoveUnitsToTargetPositionEvent()
    {
        object[] content = new object[] { new Vector3(10.0f, 2.0f, 5.0f), 1, 2, 5, 10 }; // Array contains the target position and the IDs of the selected units
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
        PhotonNetwork.RaiseEvent(MoveUnitsToTargetPositionEventCode, content, raiseEventOptions, SendOptions.SendReliable);
    }
    public void OnEvent(EventData photonEvent)
    {
        byte eventCode = photonEvent.Code;

    }
    //=========================================
    //
    //=========================================
    public void MatchToRoom()
    {


        if (PhotonNetwork.IsConnected)
        {
            //maybe add another if statement saying if in a room, then leave it and try again once connected to lobby
            if (!PhotonNetwork.InRoom || PhotonNetwork.InLobby)
            {
                //  PhotonNetwork.JoinRandomRoom();
                // Debug.Log("Room Joined");
                GetOpenRoom();
                if (desiredRoom != "")
                {
                    PhotonNetwork.JoinRoom(desiredRoom);
                }
                else
                {
                    CreateRoom();
                }
            }
            else
            {
                Debug.LogError("Can't join room now, client is not ready");
            }


        }
    }
    //=========================================
    //
    //=========================================
    public void CreateRoom()
    {
        RoomOptions roomOptions = new RoomOptions();

        maxPlayers = 5;
                       //Debug.Log( maxPlayers );
        roomOptions.MaxPlayers = maxPlayers;

        //roomOptions.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable();
        //roomOptions.CustomRoomProperties.Add("r",)
        string roomName = BuildRandomString();
        PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, null);
        roomTextDisplay.text = roomName;
        Debug.Log("Room Creation Attempted");

    }

    //=========================================
    //
    //=========================================
    public void LeaveRoom()
    {

        PhotonNetwork.LeaveRoom();


        Debug.Log("Room Left");
    }

    //=========================================
    //
    //=========================================
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        base.OnJoinRandomFailed(returnCode, message);
    }

    //=========================================
    //
    //=========================================
    public override void OnDisconnected(DisconnectCause cause)
    {

        Debug.Log("Disconnected From Photon: " + cause.ToString());
        base.OnDisconnected(cause);
        switch (cause)
        {
            case DisconnectCause.None:
                break;
            case DisconnectCause.ExceptionOnConnect:
                break;
            case DisconnectCause.DnsExceptionOnConnect:
                break;
            case DisconnectCause.ServerAddressInvalid:
                break;
            case DisconnectCause.Exception:
                break;
            case DisconnectCause.ServerTimeout:
                break;
            case DisconnectCause.ClientTimeout:
                break;
            case DisconnectCause.DisconnectByServerLogic:
                break;
            case DisconnectCause.DisconnectByServerReasonUnknown:
                break;
            case DisconnectCause.InvalidAuthentication:
                break;
            case DisconnectCause.CustomAuthenticationFailed:
                break;
            case DisconnectCause.AuthenticationTicketExpired:
                break;
            case DisconnectCause.MaxCcuReached:
                break;
            case DisconnectCause.InvalidRegion:
                break;
            case DisconnectCause.OperationNotAllowedInCurrentState:
                break;
            case DisconnectCause.DisconnectByClientLogic:
                break;
            case DisconnectCause.DisconnectByOperationLimit:
                break;
            case DisconnectCause.DisconnectByDisconnectMessage:
                break;
        }

        

    }

    //=========================================
    //
    //=========================================
    public override void OnLeftRoom()
    {
        Debug.Log("We Left room");
        base.OnLeftRoom();
        roomInfos.Clear();
    }

    //=========================================
    //
    //=========================================
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        base.OnRoomListUpdate(roomList);

        roomInfos = roomList;

    }

    //=========================================
    //
    //=========================================
    public void GetOpenRoom()
    {
        desiredRoom = "";
        foreach (RoomInfo room in roomInfos)
        {
            Debug.Log("the room name is " + room.Name);
 
        }
    }

    //=========================================
    //
    //=========================================
    string BuildRandomString()
    {
        int length = 7;

        // creating a StringBuilder object()
        StringBuilder str_build = new StringBuilder();
        System.Random random = new System.Random();

        char letter;

        for (int i = 0; i < length; i++)
        {
            double flt = random.NextDouble();
            int shift = Convert.ToInt32(Math.Floor(25 * flt));
            letter = Convert.ToChar(shift + 65);
            str_build.Append(letter);
        }
        return str_build.ToString();
    }

    //=========================================
    //
    //=========================================
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);


        Debug.Log("Error" + returnCode + " -We failed to create room " + message);
    
    }

    //=========================================
    //
    //=========================================
    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        base.OnJoinRoomFailed(returnCode, message);
        roomInfos.Clear();
        Debug.Log("Error" + returnCode + " -We failed to join room " + message);
 
    }

}

